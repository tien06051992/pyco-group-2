 if (!Enabler.isInitialized()) {
            Enabler.addEventListener(
                studio.events.StudioEvent.INIT,
                enablerInitialized);
        } else {
            enablerInitialized();
        }

        function enablerInitialized() {
            // Enabler initialized.
            // In App ads are rendered offscreen so animation should wait for
            // the visible event. These are simulated with delays in the local
            // environment.
            if (!Enabler.isVisible()) {
                Enabler.addEventListener(
                    studio.events.StudioEvent.VISIBLE,
                    adVisible);
            } else {
                adVisible();
            }
        }

        function adVisible() {
            // Ad visible, start ad/animation.
        }